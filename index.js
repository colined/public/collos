const lodash = require('lodash');
const colors = require('colors');

const log = require('./modules/log');
const Fs = require('./modules/fs');
const Render = require('./modules/render');
const Utils = require('./modules/utils');

const server = require('./modules/server');
const builder = require('./modules/builder');

const DefaultConfig = require('./modules/default-config');

const config = Utils.Config.getAppConfig({
    path: './',
    out: './.out',
    listen: 'localhost:8000',
    config: 'config.json',
    server: false
});

const defaults = new DefaultConfig();

const fs = new Fs(config.path);
const render = new Render(fs, config);

console.log(colors.gray('Root  :'), config.path);

if (config.server) {
    const listen = Utils.Config.parseListen(config.listen);
    server.start(fs, render, listen, config, defaults, () => {
        if (listen.hostname && listen.hostname.length > 0) {
            console.log(colors.gray('Server:'), listen.hostname + ':' + listen.port);
        } else {
            console.log(colors.gray('Server:'), listen.port);
        }
    });
} else {
    console.log(colors.gray('Out   :'), config.out);
    builder.process(fs, log, render, config, defaults).catch(err => {
        console.error(err);
        process.exit(1);
    });
}
