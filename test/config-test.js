const assert = require('assert');
const mockfs = require('mock-fs');
const Fs = require('../modules/fs');
const Config = require('../modules/config');

const defaults = {
    foo: 'FOO',
    bar: 'bar'
};

describe('Config Manager', () => {
    before(() => {
        mockfs({
            'root': {
                'config.json': '{ bar: "Bar" }',
                'dir': {
                    'config.json': '{ bar: "baR" }',
                },
                'bad': {
                    'config.json': 'MAILFORMED_JSON',
                }
            }
        });
    })

    after(mockfs.restore)

    it('Only defaults', () => {
        const c = new Config(new Fs(''), 'config.json', defaults);
        return c.resolve('test').then(config => {
            assert.equal(config.foo, 'FOO');
            assert.equal(config.bar, 'bar');
            assert.equal(config.baz, undefined);
        });
    })

    it('Simple config', () => {
        const c = new Config(new Fs(''), 'config.json', defaults);
        return c.resolve('root').then(config => {
            assert.equal(config.foo, 'FOO');
            assert.equal(config.bar, 'Bar');
            assert.equal(config.baz, undefined);
        });
    })

    it('Read uplevel config', () => {
        const c = new Config(new Fs(''), 'config.json', defaults);
        return c.resolve('root/root').then(config => {
            assert.equal(config.foo, 'FOO');
            assert.equal(config.bar, 'Bar');
            assert.equal(config.baz, undefined);
        });
    })

    it('Read uplevel configs with overwrite', () => {
        const c = new Config(new Fs(''), 'config.json', defaults);
        return c.resolve('root/dir').then(config => {
            assert.equal(config.foo, 'FOO');
            assert.equal(config.bar, 'baR');
            assert.equal(config.baz, undefined);
        });
    })

    it('Invalid config', () => {
        const c = new Config(new Fs(''), 'config.json', defaults);
        return c.resolve('root/bad').then((data) => {
            assert(false);
        }).catch(err => {
            assert.equal(err.name, 'ParseConfigError');
        });
    })
})
