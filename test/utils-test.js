const assert = require('assert');
const Utils = require('../modules/utils');

describe('Path utils', () => {
    it('Join', () => {
        assert.equal(Utils.Path.join('test'), 'test');
        assert.equal(Utils.Path.join('test', 'test1'), 'test/test1');
        assert.equal(Utils.Path.join('test', 'test1', 'test2'), 'test/test1/test2');
        assert.equal(Utils.Path.join('', 'test'), 'test');
        assert.equal(Utils.Path.join('', 'test', 'test1'), 'test/test1');
        assert.equal(Utils.Path.join(null, 'test'), 'test');
        assert.equal(Utils.Path.join(null, 'test', 'test1'), 'test/test1');
        assert.equal(Utils.Path.join('test', null, 'test1'), 'test/test1');
        assert.equal(Utils.Path.join('test', '/', '/test1'), 'test/test1');
    })

    it('Parse', () => {
        assert.deepEqual(Utils.Path.parse(''), { basepath: '/', path: '' });
        assert.deepEqual(Utils.Path.parse('/'), { basepath: '/', path: '' });
        assert.deepEqual(Utils.Path.parse('test'), { basepath: '/', path: 'test' });
        assert.deepEqual(Utils.Path.parse('test/'), { basepath: '/', path: 'test' });
        assert.deepEqual(Utils.Path.parse('/test/'), { basepath: '/', path: 'test' });
        assert.deepEqual(Utils.Path.parse('abc/test'), { basepath: '/abc', path: 'test' });
        assert.deepEqual(Utils.Path.parse('abc/test/'), { basepath: '/abc', path: 'test' });
        assert.deepEqual(Utils.Path.parse('/abc/test'), { basepath: '/abc', path: 'test' });
        assert.deepEqual(Utils.Path.parse('/abc/test/'), { basepath: '/abc', path: 'test' });
        assert.deepEqual(Utils.Path.parse('abc/xyz/test'), { basepath: '/abc/xyz', path: 'test' });
        assert.deepEqual(Utils.Path.parse('abc/xyz/test/'), { basepath: '/abc/xyz', path: 'test' });
        assert.deepEqual(Utils.Path.parse('/abc/xyz/test'), { basepath: '/abc/xyz', path: 'test' });
        assert.deepEqual(Utils.Path.parse('/abc/xyz/test/'), { basepath: '/abc/xyz', path: 'test' });
    })

    it('Split', () => {
        assert.deepEqual(Utils.Path.split('/'), ['/']);
        assert.deepEqual(Utils.Path.split('/a'), ['/a', '/']);
        assert.deepEqual(Utils.Path.split('/a/'), ['/a', '/']);
        assert.deepEqual(Utils.Path.split('/a/b'), ['/a/b', '/a', '/']);

        assert.deepEqual(Utils.Path.split(''), ['/']);
        assert.deepEqual(Utils.Path.split('a'), ['/a', '/']);
        assert.deepEqual(Utils.Path.split('a/'), ['/a', '/']);
        assert.deepEqual(Utils.Path.split('a/b'), ['/a/b', '/a', '/']);
    });

    it('Rename', () => {
        assert.equal(Utils.Path.rename('abc', 'test.txt'), 'test.txt');
        assert.equal(Utils.Path.rename('abc/abc', 'test.txt'), 'abc/test.txt');
        assert.equal(Utils.Path.rename('abc/abc.abc', 'test.txt'), 'abc/test.txt');
    })

    it('Rename Extension', () => {
        assert.equal(Utils.Path.renameExt('abc', '.xyz'), 'abc.xyz');
        assert.equal(Utils.Path.renameExt('abc/abc', '.xyz'), 'abc/abc.xyz');
        assert.equal(Utils.Path.renameExt('abc/abc.abc', '.xyz'), 'abc/abc.xyz');
    })

    it ('Relative', () => {
        assert.equal(Utils.Path.relative('/a/b', '/a/b'), '');
        assert.equal(Utils.Path.relative('/a/b/', '/a/b'), '');
        assert.equal(Utils.Path.relative('/a/b', '/a/b/'), '');
        assert.equal(Utils.Path.relative('/a/b', '/a'), 'b');
        assert.equal(Utils.Path.relative('/a/b', '/a/c'), '../b');
        assert.equal(Utils.Path.relative('/a/b', '/c'), '../a/b');
        assert.equal(Utils.Path.relative('/a/b', '/a/c/d'), '../../b');
        assert.equal(Utils.Path.relative('/a/b', '/c/d'), '../../a/b');
        assert.equal(Utils.Path.relative('/a/b', '/c/d/'), '../../a/b');
        assert.equal(Utils.Path.relative('/a/b', '/a/b/c/d'), '../..');
    });
})

describe('Config utils', () => {
    it('Merge', () => {
        assert.deepEqual(
            Utils.Config.merge(
                [{ foo: 'FOO', baz: 'BAZ' }], 
                { foo: 'foo', bar: 'bar' }), 
            { foo: 'FOO', bar: 'bar' });
    })
})

describe('Match', () => {
    it('Match', () => {
        assert.equal(Utils.Match.match('/', 'test', ['/**/*.']), true);
        assert.equal(Utils.Match.match('/', 'logo.png', ['/**/.*/']), false);
        assert.equal(Utils.Match.match('/static', '_test.scss', ['/static/_*.scss']), true);
        assert.equal(Utils.Match.matchDir('/node_modules', ['/**/*/']), true);
        assert.equal(Utils.Match.matchDir('/node_modules', ['/node_modules/']), true);
    });
});