const assert = require('assert');
const mockfs = require('mock-fs');
const Resolver = require('../modules/resolver');
const Item = require('../modules/item');
const DefaultConfig = require('../modules/default-config');
const Fs = require('../modules/fs');

const config = new DefaultConfig();

describe('Item Resolver', () => {
    before(() => {
        mockfs({
            'root': {
                'index.html': '',
                'article.md': '',
                'logo.png': '',
                'styles': {
                    'style.css': ''
                },
                'docs': {
                    'article2.md': '',
                    'dir': {
                        'index.html': '',
                        'article3.md': '',
                        'template.html': ''
                    }
                }
            }
        });
    })

    after(mockfs.restore)

    it('Base path not found', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/test', 'test.html', config).then(item => {
            assert.equal(item.type, Item.Type.NOT_FOUND);
        });
    })

    it('Item not found', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/', 'test.html', config).then(item => {
            assert.equal(item.type, Item.Type.NOT_FOUND);
        });
    })

    it('Item not found in folder', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/docs', 'test.html', config).then(item => {
            assert.equal(item.type, Item.Type.NOT_FOUND);
        });
    })

    it('Static files', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/', 'logo.png', config).then(item => {
            assert.equal(item.type, Item.Type.STATIC);
        });
    })

    it('Static files from folder', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/styles', 'style.css', config).then(item => {
            assert.equal(item.type, Item.Type.STATIC);
        });
    })

    it('Template', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/', 'article.html', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, '/article.md');
            assert.equal(item.template, true);
        });
    })

    it('Template from folder', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/docs', 'article2.html', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, '/docs/article2.md');
            assert.equal(item.template, true);
        });
    })

    it('Template from folders', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/docs/dir', 'article3.html', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, '/docs/dir/article3.md');
            assert.equal(item.template, '/docs/dir/template.html');
        });
    })

    it('Index', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/', 'index.html', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, null);
            assert.equal(item.template, '/index.html');
        });
    })

    it('Index from folders', () => {
        const r = new Resolver(new Fs('root'));
        return r.resolve('/docs/dir', 'index.html', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, null);
            assert.equal(item.template, '/docs/dir/index.html');
        });
    })

    it('Item without extension', () => {
        const config = {
            templates: [
                {
                    pattern: ['/**/*.'],
                    content: '.md',
                    template: true

                }
            ]
        };

        const r = new Resolver(new Fs('root'));
        return r.resolve('/', 'article', config).then(item => {
            assert.equal(item.type, Item.Type.TEMPLATE);
            assert.equal(item.source, '/article.md');
            assert.equal(item.template, true);
        });
    });
})