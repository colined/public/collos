collos
======

**Simple** static site generator

Getting started
---------------
```
git clone git@gitlab.com:colined/collos.git
cd collos
npm install
npm start -- --server
```
Visit to http://localhost:8000/README.html and see this [README](http://localhost:8000/README.html)

Supported Markdown
------------------
Rendering using [markdown-it](https://github.com/markdown-it/markdown-it)
library with some extensions. Please see [documentation](/markdown.html) with
supported markdown samples.