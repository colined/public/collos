'use strict'

const fs = require('fs');
const minimatch = require('minimatch');

class PathError extends Error {
    constructor(path, ...params) {
        super(...params);

        Error.captureStackTrace(this, PathError);

        this.path = path;
    }
}

function join(root, path) {
    if (!path || path.length === 0) {
        return root;
    }
    
    if (!root || root.length === 0) {
        return path;
    }

    if (root.charAt(root.length - 1) === '/') {
        if (path.charAt(0) === '/') {
            return root + path.substr(1);
        } else {
            return root + path;
        }
    } else if (path.charAt(0) === '/') {
        return root + path;
    } else {
        return root + '/' + path;
    }
}

function indexOfOrLast(str, c, start) {
    const pos = str.indexOf(c, start);
    if (pos === -1)
        return str.length;
    return pos;
}

function getNextPathPart(path1, path2, start) {
    if (start >= path1.length || start >= path2.length)
        return -1;

    const pos1 = indexOfOrLast(path1, '/', start);
    const pos2 = indexOfOrLast(path2, '/', start);
    if (pos1 !== pos2)
        return -1;
    return pos1;
}

module.exports.Path = {
    join: function(root, ...parts) {
        var result = root;
        parts.forEach(part => {
            result = join(result, part);
        });

        return result;
    },

    parse: function(path) {
        if (path.length === 0) {
            return { basepath: '/', path: '' };
        }

        const end = path[path.length - 1] === '/' ? path.length - 1 : path.length;
        const pos = path.lastIndexOf('/', end - 1);
        if (pos < 1) {
            if (pos >= end) {
                return { basepath: '/', path: '' }
            }
            return { basepath: '/', path: path.substring(pos + 1, end) }
        }

        const start = path[0] === '/' ? 1 : 0;
        return { basepath: '/' + path.substring(start, pos), path: path.substring(pos + 1, end) };
    },

    split: function(path) {
        if (path === '' || path === '/') {
            return ['/'];
        }

        const start = path.indexOf('/') === 0 ? 1 : 0;

        let end = path.length;
        if (path.lastIndexOf('/', end) === end - 1)
            end -= 1;

        const result = [];
        while (end > start) {
            result.push('/' + path.substring(start, end));
            end = path.lastIndexOf('/', end - 1);
        }
        result.push('/');

        return result;
    },

    rename: function(path, name) {
        const slash = path.lastIndexOf('/');
        if (slash < 0)
            return name;
        if (slash >= path.length - 1)
            return path;
        return path.substr(0, slash + 1) + name;
    },

    getName: function(path) {
        const slash = path.lastIndexOf('/');
        if (slash < 0)
            return path;
        if (slash >= path.length - 1)
            return '';
        return path.substr(slash);
    },

    renameExt: function(path, ext) {
        const slash = path.lastIndexOf('/');
        const dot = path.lastIndexOf('.');
        if (dot <= slash)
            return path + ext;

        return path.substr(0, dot) + ext;
    },

    getExt: function(path) {
        const slash = path.lastIndexOf('/');
        const dot = path.lastIndexOf('.');
        if (dot <= slash)
            return '';
        return path.substr(dot);
    },

    relative: function(path, basepath) {
        if (!path || path.length === 0 || path[0] !== '/')
            throw new PathError(path, `"${path}" should be absolute path`)
        if (!basepath || basepath.length === 0 || basepath[0] !== '/')
            throw new PathError(path, `"${basepath}" should be absolute path`)

        let lastPos = 1;
        while (lastPos >= 0) {
            const pos = getNextPathPart(path, basepath, lastPos);
            // console.log(lastPos, pos, path, basepath, path.substr(lastPos, pos - lastPos), basepath.substr(lastPos, pos - lastPos));
            if (pos === -1)
                break;

            if (path.substr(lastPos, pos - lastPos) !== basepath.substr(lastPos, pos - lastPos))
                break;

            lastPos = pos + 1;
        }

        if (lastPos >= basepath.length)
            return path.substr(lastPos);

        let levels = '..';
        let lastSlash = false;
        for (let i = lastPos; i < basepath.length - 1; ++i) {
            if (basepath[i] === '/') {
                if (!lastSlash) {
                    levels += '/..';
                }
                lastSlash = true;
            } else {
                lastSlash = false;
            }
        }

        if (lastPos >= path.length)
            return levels;

        return levels + '/' + path.substr(lastPos);
    }
}

function fixExt(path) {
    const slash = path.lastIndexOf('/');
    const dot = path.lastIndexOf('.');
    if (dot <= slash && slash < path.length - 1)
        return path + '.';
    return path;
}

module.exports.Match = {
    match: function(basepath, path, patterns) {
        if (!patterns)
            return false;

        const pathname = fixExt(module.exports.Path.join('/', basepath, path));
        basepath = fixExt(basepath);
        for (let i = 0; i < patterns.length; ++i) {
            const pattern = patterns[i];
            if (pattern.charAt(pattern.length - 1) === '/') {
                if (minimatch(basepath, pattern.substr(0, pattern.length - 1))) {
                    return true;
                }
            } else {
                if (minimatch(pathname, pattern)) {
                    return true;
                }
            }
        }

        return false;
    },

    matchDir: function(basepath, patterns) {
        if (!patterns)
            return false;

        // basepath = fixExt(basepath);
        for (let i = 0; i < patterns.length; ++i) {
            const pattern = patterns[i];
            if (pattern.charAt(pattern.length - 1) === '/') {
                if (minimatch(basepath, pattern.substr(0, pattern.length - 1)))
                    return true;
            }
        }

        return false;
    }
}

function applyEnvVars(obj) {
    for (let i in obj) {
        if (typeof obj[i] === 'object') {
            obj[i] = replaceEnvVars(obj[i]);
        } else if (typeof obj[i] === 'string') {
            obj[i] = obj[i].replace(/\$([A-Za-z0-9\-_]+)/g, function(match, arg) {
                if (arg in process.env) {
                    return process.env[arg];
                } else {
                    return '$' + arg;
                }
            });
        }
    }
    return obj;
};

function findValue(key, configs, value) {
    for (let i = 0; i < configs.length; ++i) {
        const config  = configs[i];
        if (config !== null && (key in config)) {
            return config[key];
        }
    }

    return value;
}

function isOption(arg) {
    return arg && arg.length > 2 && arg.charAt(0) === '-' && arg.charAt(1) === '-';
}

function parseCommandLine(argv) {
    const result = { _argv: [] };

    for (let i = 0; i < argv.length; i += 1) {
        const arg = argv[i];
        if (!isOption(arg)) {
            result._argv.push(arg);
            continue;
        }

        const nextArg = (i === argv.length - 1) ? null : argv[i + 1];
        if (nextArg === null || isOption(nextArg)) {
            result[arg.substr(2)] = true;
        } else {
            result[arg.substr(2)] = nextArg;
            i += 1;
        }
    }

    return result;
}

function readFileSync(fileName) {
    try {
        fs.accessSync(fileName, fs.R_OK);
        return fs.readFileSync(fileName, 'utf8');
    } catch (e) {
        return null;
    }    
}

module.exports.Config = {
    merge: function(configs, defaults) {
        const result = {};
        Object.keys(defaults).forEach(key => {
            result[key] = findValue(key, configs, defaults[key]);
        });

        return result;
    },

    parse: function(text, useEnvVars) {
        const config = eval('\"use string\";var d=' + text + ';d') || null;
        if (!useEnvVars || !config)
            return config;
        return applyEnvVars(config);
    },

    parseListen: function(value) {
        if (typeof value == 'number')
            return { hostname: '', port: value };

        let port = parseInt(value);
        if (!isNaN(port))
            return { hostname: '', port: parseInt(port) };
        const pair = value.split(':');
        return { hostname: pair[0], port: parseInt(pair[1]) };
    },

    getAppConfig(defaults) {
        const configs = [];
        const commandLine = parseCommandLine(process.argv.slice(2));
        configs.push(commandLine);

        if (process.env.NODE_ENV)
            configs.push(this.parse(readFileSync('local-' + process.env.NODE_ENV + '.json', true)));
        configs.push(this.parse(readFileSync('local.json', true)));

        if (process.env.NODE_ENV)
            configs.push(this.parse(readFileSync(process.env.NODE_ENV + '.json', true)));
        configs.push(this.parse(readFileSync('config.json', true)));

        return this.merge(configs, defaults);
    }
}
