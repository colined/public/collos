const parse5 = require('parse5');

function findTag(nodes, tag) {
    if (!nodes)
        return null;

    for (let i = 0; i < nodes.length; ++i) {
        const node = nodes[i];

        if (!node)
            continue;

        if (node.tagName && node.tagName.toLowerCase() === tag) {
            return node;
        }

        const child = findTag(node.childNodes, tag);
        if (child !== null)
            return child;
    }

    return null;
}

module.exports = function(content) {
    const doc = parse5.parse(content);
    const body = findTag(doc.childNodes, 'body');
    const title = findTag(doc.childNodes, 'title');

    if (body) {
        return {
            meta: {
                title: title === null ? '' : parse5.serialize(title)
            },
            body: parse5.serialize(body)
        };
    }

    return {
        meta: {},
        body: ''
    };
}

module.exports.extension = '.html';
