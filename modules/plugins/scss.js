const sass = require('node-sass');
const pathParse = require('path').parse;
const Utils = require('../utils');

module.exports = function(content, path, options) {
    return sass.renderSync({
        data: content,
        indentedSyntax: false,
        importer: function(url, prev, done) {
            const prevParsed = pathParse(prev);
            const parsed = pathParse(url);
            const file = Utils.Path.join(prevParsed.dir, parsed.dir, '_' + parsed.base + '.scss');

            const contents = options.fs.readFileSync(file, 'utf8')
            return { file, contents };
        }
    }).css;
}

module.exports.extension = '.scss';
