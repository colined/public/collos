const parse5 = require('parse5');

function traverseTags(nodes, regex, callback) {
    if (!nodes)
        return null;

    for (let i = 0; i < nodes.length; ++i) {
        const node = nodes[i];

        if (!node)
            continue;

        if (node.tagName && regex.test(node.tagName.toLowerCase())) {
            const result = callback(node);
            if (result)
                return node;
        }

        const child = traverseTags(node.childNodes, regex, callback);
        if (child !== null)
            return child;
    }

    return null;
}

function getAttr(attrs, name) {
    for (let i = 0; i < attrs.length; ++i) {
        const attr = attrs[i];
        if (attr.name === name)
            return attr.value;
    }

    return null;
}

function addClass(attrs, cls) {
    for (let i = 0; i < attrs.length; ++i) {
        const attr = attrs[i];
        if (attr.name === 'class') {
            attr.value += ' ' + cls;
            return;
        }
    }

    attrs.push({
        name: 'class',
        value: cls
    });
}

function buildToc(content, levels, minlevel, tag) {
    levels = levels || 2;
    minlevel = minlevel || 2;
    tag = tag || 'ul';

    const headers = [];
    const doc = parse5.parse(content);
    let level = -1;
    traverseTags(doc.childNodes, /^h[1-6]$/, node => {
        const l = parseInt(node.tagName.toLowerCase().replace('h', '')) - minlevel;
        if (l < 0 || l >= levels)
            return;

        const text = parse5.serialize(node);
        if (l > level) {
            const count = l - level;
            for (let i = 0; i < count; ++i)
                headers.push('<', tag, '>');
        } else if (l < level) {
            const count = level - l;
            for (let i = 0; i < count; ++i)
                headers.push('</li></', tag, '>');
        } else {
            headers.push('</li>');
        }

        level = l;

        const id = getAttr(node.attrs, 'id');

        if (id === null) {
            headers.push('<li><a href="#">', text, '</a>');
        } else {
            headers.push('<li><a href="#' + id + '">', text, '</a>');
        }
    });
    for (let i = 0; i <= level; ++i)
        headers.push('</li></', tag, '>');

    return headers.join('');
}

function selectLink(content, cls, path) {
    const doc = parse5.parse(content);
    traverseTags(doc.childNodes, /^a$/, node => {
        const href = getAttr(node.attrs, 'href');
        if (href === path) {
            addClass(node.attrs, cls);
            return true;
        }
    });    

    return parse5.serialize(doc);
}

module.exports = function(path, options) {
    return {
        build: function(content, levels, minlevel, tag) {
            return buildToc(content, levels, minlevel, tag);
        },

        select: function(content, cls, p) {
            return selectLink(content, cls, p || path);
        }
    }
}

module.exports.namespace = 'toc';
