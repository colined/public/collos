const parse5 = require('parse5');
const Utils = require('../utils');

function traverseTags(nodes, regex, callback) {
    if (!nodes)
        return null;

    for (let i = 0; i < nodes.length; ++i) {
        const node = nodes[i];

        if (!node)
            continue;

        if (node.tagName && regex.test(node.tagName.toLowerCase())) {
            const result = callback(node);
            if (result)
                return node;
        }

        const child = traverseTags(node.childNodes, regex, callback);
        if (child !== null)
            return child;
    }

    return null;
}

function getAttr(attrs, name) {
    for (let i = 0; i < attrs.length; ++i) {
        const attr = attrs[i];
        if (attr.name === name)
            return attr;
    }

    return null;
}

module.exports = function(path, options) {
    return {
        read: function(path) {
            return options.fs.readFileSync(path, 'utf8');
        },
        parse: function(content, extension) {
            if (!extension) {
                extension = Utils.Path.getExt(content.toLowerCase());
                content = options.fs.readFileSync(content, 'utf8');
            }
            return options.parse(content, extension);
        },
        relative: function(content) {
            if (!content || content.length === 0)
                return '';

            if (content[0] === '/')
                return Utils.Path.relative(content, Utils.Path.parse(path).basepath);

            const doc = parse5.parse(content);
            traverseTags(doc.childNodes, /^a$/, node => {
                const href = getAttr(node.attrs, 'href');
                if (href && href.value && href.value.length > 0 && href.value[0] === '/') {
                    href.value = Utils.Path.relative(href.value, Utils.Path.parse(path).basepath);
                }

                return false;
            });    

            return parse5.serialize(doc);

        }
    }
}

module.exports.namespace = 'core';
