const sass = require('node-sass');
const pathParse = require('path').parse;
const Utils = require('../utils');

module.exports = function(content, path, options) {
    return sass.renderSync({
        data: content,
        indentedSyntax: true,
        importer: function(url, prev, done) {
            const prevParsed = pathParse(prev);
            const parsed = pathParse(url);
            const file = Utils.Path.join(prevParsed.dir, parsed.dir, '_' + parsed.base + '.sass');

            const contents = options.fs.readFileSync(file, 'utf8')
            return { file, contents };
        }
    }).css;
}

module.exports.extension = '.sass';
