const fm = require('front-matter');
const highlightjs = require('highlight.js');
const transliteration = require('transliteration');
const md = require('markdown-it')({
    html: true,
    breaks: false,
    linkify: true,
    typographer: true,
    highlight: function(str, lang) {
        if (lang && highlightjs.getLanguage(lang)) {
            try {
                return '<pre class="hljs"><code class="hljs">' + 
                    highlightjs.highlight(str, { language: lang }).value +
                    '</pre></code>';
            } catch (err) {

            }
        }

        return '';
    }
}).use(require('markdown-it-video'))
    .use(require('markdown-it-container'), 'warning')
    .use(require('markdown-it-container'), 'note')
    .use(require('markdown-it-container'), 'info')
    .use(require('markdown-it-container'), 'success')
    .use(require('markdown-it-emoji'))
    .use(require('markdown-it-footnote'))
    .use(require('markdown-it-mark'))
    .use(require('markdown-it-sub'))
    .use(require('markdown-it-sup'))
    .use(require('markdown-it-imsize'), { autofill: true })
    .use(require('markdown-it-block-image'), { outputContainer: 'div' })
    .use(require('markdown-it-anchor'), { slugify: transliteration.slugify });

module.exports = function(content) {
    const data = fm(content);
    const body = md.render(data.body);
    return {
        meta: data.attributes,
        body
    }
}

module.exports.extension = '.md';
