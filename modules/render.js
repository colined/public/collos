const lodash = require('lodash');
const fs = require('fs');

const Item = require('./item');
const Plugins = require('./plugins');
const Utils = require('./utils');

class DefaultTemplate {
    constructor(filename) {
        this.filename = filename;
        this.path = '/';
        this.content = fs.readFileSync(require.resolve(this.filename), 'utf8');
    }

    read(fs) {
        return new Promise((resolve, reject) => { return resolve(this.content); });
    }
}

class FileTemplate {
    constructor(filename) {
        this.filename = filename;
        this.path = filename;
    }

    read(fs) {
        return fs.readFile(this.filename, 'utf8');
    }
}

function getTemplate(item, defaults) {
    if (item === null)
        return defaults.error;

    switch (item.type) {
    case Item.Type.ERROR:
        return defaults.error;
    case Item.Type.NOT_FOUND:
        return defaults.notfound;
    case Item.Type.TEMPLATE:
        if (item.template === true)
            return defaults.template;
        if (item.template)
            return new FileTemplate(item.template);
        return null;
    }

    return defaults.error;
}

function resolveTemplate(fs, item, defaults) {
    return new Promise((resolve, reject) => {
        var template = getTemplate(item, defaults);
        if (template === null)
            return resolve({ content : null, path: null });
        return template.read(fs).then(content => resolve({ content, path: template.path })).catch(reject);
    });
}

function resolveContent(item, fs, path, plugins) {
    return new Promise((resolve, reject) => {
        if (item === null || item.source === null)
            return resolve(null);

        return fs.readFile(item.source, 'utf8').then(content => {
            try {
                const ext = Utils.Path.getExt(item.source).toLowerCase();
                resolve(plugins.parseContent(content, ext, fs, path));
            } catch (err) {
                reject(err);
            }
        }).catch(reject);
    });
}

function render(template, content, path, error, imports) {
    if (template === null)
        return content;

    const compiled = lodash.template(template, { imports });
    return compiled({ content: content, context: { path, error } });
}

class Render {
    constructor(fs, config) {
        this.fs = fs;
        this.plugins = new Plugins(this);
        this.plugins.register(require('./plugins/markdown'));
        this.plugins.register(require('./plugins/html'));
        this.plugins.register(require('./plugins/sass'));
        this.plugins.register(require('./plugins/scss'));
        this.plugins.register(require('./plugins/core'));
        this.plugins.register(require('./plugins/toc'));
        this.defaults = {
            error: new DefaultTemplate('./html/error.html'),
            notfound: new DefaultTemplate('./html/notfound.html'),
            template: new DefaultTemplate('./html/template.html'),
        }
    }

    render(item, path, error) {
        const data = { template: null, content: null };
        return resolveTemplate(this.fs, item, this.defaults).then(template => {
            data.template = template;
            return resolveContent(item, this.fs, path, this.plugins);
        }).then(content => {
            data.content = content;
            const imports = this.plugins.getImports(this.fs, data.template.path, path, this);
            return render(data.template.content, data.content, path, error, imports);
        });
    }
}

module.exports = Render;
