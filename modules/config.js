const Utils = require('./utils');

class ParseConfigError extends Error {
    constructor(message, filepath, err) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);

        this.filepath = filepath;
        this.err = err;
    }
}

class Config {
    constructor(fs, filename, defaults) {
        this.fs = fs;
        this.filename = filename;
        this.defaults = defaults;
    }

    resolve(basepath) {
        const promises = Utils.Path.split(basepath).map(subpath => {
            const path = Utils.Path.join(subpath, this.filename);
            return this.fs.readFile(path, 'utf8').catch(err => {
                return null;
            }).then(data => {
                try {
                    return Utils.Config.parse(data);
                } catch (err) {
                    throw new ParseConfigError(`Cannot parse config "${path}": ${err.message}`, path, err);
                }
            });
        });

        return Promise.all(promises).then(configs => {
            return Utils.Config.merge(configs, this.defaults);
        });
    }
}

module.exports = Config;
