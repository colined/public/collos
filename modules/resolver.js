const Item = require('./item');
const Utils = require('./utils');

function checkStatic(fs, basepath, path, static) {
    return new Promise((resolve, reject) => {
        if (!Utils.Match.match(basepath, path, static)) {
            return resolve(null);
        }

        const fullpath = Utils.Path.join(basepath, path);
        return fs.stat(fullpath).then(stat => {
            if (!stat.isFile()) {
                return resolve(null);
            }

            return resolve(new Item(Item.Type.STATIC, fullpath));
        }).catch((err) => {
            resolve(null);
        });
    });
}

function getFullTempaltePath(basepath, template) {
    if (template.charAt(0) === '/')
        return template;

    return Utils.Path.join(basepath, template);
}

function resolveContent(fs, basepath, path, content) {
    return new Promise((resolve, reject) => {
        if (!content) {
            return resolve(null);
        }

        const fullpath = Utils.Path.renameExt(Utils.Path.join(basepath, path), content);
        return fs.stat(fullpath).then((stat) => {
            if (!stat.isFile()) {
                return reject();
            }
            return resolve(fullpath);
        }).catch(reject);
    });
}

function resolveTemplate(fs, basepath, template) {
    return new Promise((resolve, reject) => {
        if (template === true || template === false) {
            resolve(template);
        }

        if (!template) {
            resolve(null)
        }

        const fullpath = getFullTempaltePath(basepath, template);
        return fs.stat(fullpath).then((stat) => {
            if (!stat.isFile()) {
                return reject();
            }
            return resolve(fullpath);
        }).catch(reject);
    });
}

function checkTemplate(fs, basepath, path, template) {
    return new Promise((resolve, reject) => {
        if (!Utils.Match.match(basepath, path, template.pattern)) {
            return reject();
        }

        let content = null;
        return resolveContent(fs, basepath, path, template.content).then(result => {
            content = result;
            return resolveTemplate(fs, basepath, template.template);
        }).then(result => {
            return new Item(Item.Type.TEMPLATE, content, result);
        }).then(resolve).catch(reject);
    });
}

function checkTemplates(fs, basepath, path, templates) {
    return new Promise((resolve, reject) => {
        function next(index) {
            if (index === templates.length) {
                return reject();
            }

            checkTemplate(fs, basepath, path, templates[index]).then(item => {
                return resolve(item);
            }).catch(() => {
                return next(index + 1);
            })
        }

        return next(0);        
    });
}

class Resolver {
    constructor(fs) {
        this.fs = fs;
    }

    resolve(basepath, path, config) {
        return new Promise((resolve, reject) => {
            if (Utils.Match.match(basepath, path, config.exclude)) {
                return resolve(Item.NOT_FOUND);
            }

            this.fs.stat(basepath).then(stat => {
                if (!stat.isDirectory()) {
                    return resolve(Item.NOT_FOUND)
                }

                checkStatic(this.fs, basepath, path, config.static).then(item => {
                    if (item) {
                        return resolve(item);
                    }

                    return checkTemplates(this.fs, basepath, path, config.templates).then(resolve)
                        .catch(() => {
                            return resolve(Item.NOT_FOUND);
                        });
                })
                .catch(() => {
                    return resolve(Item.NOT_FOUND);
                });
            }).catch(() => {
                return resolve(Item.NOT_FOUND)
            });
        });
    }
}

module.exports = Resolver;
