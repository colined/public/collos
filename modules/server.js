const http = require('http');
const express = require('express');
const mime = require('mime');
const colors = require('colors');
const Item = require('./item');
const Router = require('./router');
const Utils = require('./utils');

function logEnd(data, encoding) {
    const time = Date.now() - this.logData.start;
    if (this.logData.timer !== null) {
        clearTimeout(this.logData.timer);
        this.logData.timer = null;    
    }

    let statusColor = colors.green;
    if (this.statusCode >= 500) {
        statusColor = colors.red;
    } else if (this.statusCode >= 400) {
        statusColor = colors.yellow;
    };

    switch (this.get('Resolve-Type')) {
    case 'STATIC':
        console.log(this.req.path, 
            colors.blue.bold(this.get('Resolve-Source')),
            statusColor(this.statusCode), colors.gray(time.toString() + 'ms'));
        break;
    case 'TEMPLATE':
        console.log(this.req.path, 
            colors.magenta.bold(this.get('Resolve-Source')), colors.magenta.bold(this.get('Resolve-Template')),
            statusColor(this.statusCode), colors.gray(time.toString() + 'ms'));
        break;
    case 'ERROR':
        console.log(this.req.path, 
            colors.red(this.get('Resolve-Error')),
            colors.gray(time.toString() + 'ms'));
        break;
    default:
        console.log(this.req.path, 
            statusColor(this.statusCode), colors.gray(time.toString() + 'ms'));
    }

    return this.logData.end.call(this, data, encoding);
}

function logOvertime(req, res) {
    console.log(req.path, '...');
    res.logData.timer = null;    
}

function log(req, res, next) {
    res.logData = {
        start: Date.now(),
        timer: setTimeout(() => logOvertime(req, res), 300),
        end: res.end
    };
    res.end = logEnd;
    return next();
}

function start(fs, render, listen, config, defaults, callback) {
    const app = express();
    const router = new Router(fs, config.config, defaults);

    app.get('/*', log, (req, res) => {
        router.resolve(req.path).then(item => {
            if (item.type === Item.Type.STATIC) {
                res.set('Resolve-Type', 'STATIC');
                res.set('Resolve-Source', item.source);
                res.set('Content-Type', mime.lookup(req.path));
                fs.readFile(item.source).then(buffer => {
                    return res.send(buffer);
                }).catch(err => {
                    return res.status(500).
                            set('Content-Type', 'text/plain').
                            send(err.name + ': ' + err.message + '\n' + err.stack); 
                });
            } else if (item.type === Item.Type.NOT_FOUND) {
                res.set('Resolve-Type', 'NOT_FOUND');
                const type = mime.lookup(req.path);
                if (type === 'text/html') {
                    return render.render(item, req.path).then(content => {
                        return res.status(404).
                                set('Content-Type', 'text/html').
                                send(content);
                    });
                } else {
                    return res.status(404).set('Content-Type', type).end();
                }
            } else if (item.type === Item.Type.TEMPLATE) {
                res.set('Resolve-Type', 'TEMPLATE')
                res.set('Resolve-Source', item.source).set('Resolve-Template', item.template);
                return render.render(item, req.path).then(content => {
                    const type = mime.lookup(req.path);
                    return res.status(200).
                            set('Content-Type', type).
                            send(content);
                });
            }
        }).catch(err => {
            res.set('Resolve-Type', 'ERROR');
            res.set('Resolve-Error', err.message || err.name);
            render.render(Item.ERROR, req.path, err).then(content => {
                return res.status(500).
                        set('Content-Type', 'text/html').
                        send(content); 
            }).catch(err => {
                return res.status(500).
                        set('Content-Type', 'text/plain').
                        send(err.name + ': ' + err.message + '\n' + err.stack); 
            });
        });
    });

    const server = http.createServer(app);
    server.listen(listen.port, () => {
        if (callback) {
            callback(null, listen);
        }
    });

    let rl = null;
    if (process.platform === 'win32') {
        rl = require('readline').createInterface({ input: process.stdin, output: process.stdout });
        rl.on('SIGINT', () => { process.emit('SIGINT'); });
    }

    process.on('SIGINT', () => {
        console.log(colors.gray(`Server stopped`));
        if (rl !== null) {
            rl.close();
            rl = null;
        }
        server.close();
        process.exit(0);
    });

    return true;
}

module.exports.start = start;
