const Type = {
    ERROR: 0,
    NOT_FOUND: 1,
    STATIC: 2,
    TEMPLATE: 3,
}

class Item {
    constructor(type, source, template) {
        this.type = type;
        this.source = source || null;
        this.template = template || null;
    }
}

module.exports = Item;
module.exports.Type = Type;
module.exports.ERROR = new Item(Type.ERROR);
module.exports.NOT_FOUND = new Item(Type.NOT_FOUND);
