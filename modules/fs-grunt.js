const path = require('path');

const Utils = require('./utils');

function join(root, subpath, path) {
    if (path && path.length > 0 && path.charAt(0) === '/') {
        return Utils.Path.join(root, path);
    }
    if (!subpath || subpath.length === 0) {
        return Utils.Path.join(root, path);
    }

    return Utils.Path.join(root, subpath, path);
}

class FsGrunt {
    constructor(grunt, root, subpath) {
        this.grunt = grunt;
        this.root = root;
        this.subpath = subpath;
    }

    getSubpath(subpath) {
        return new FsGrunt(this.grunt, this.root, subpath);
    }

    resolve(path) {
        return join(this.root, this.subpath, path);
    }

    readFile(path, options) {
        return new Promise((resolve, reject) => {
            try {
                const data = this.grunt.file.read(join(this.root, this.subpath, path), options || null);
                return resolve(data);
            } catch (err) {
                return reject(err);
            }
        });
    }

    stat(path) {
        return new Promise((resolve, reject) => {
            try {
                const fullName = join(this.root, this.subpath, path);
                const isFile = this.grunt.file.isFile(fullName);
                const isDir = this.grunt.file.isDir(fullName);

                const result = {
                    isDirectory: function() { return isDir; },
                    isFile: function() { return isFile; }
                };
                resolve(result);
            } catch (err) {
                return reject(err);
            }
        });
    }

    copyFile(dest, path) {
        return new Promise((resolve, reject) => {
            try {
                this.grunt.file.copy(join(this.root, this.subpath, path), dest);
                return resolve();
            } catch (err) {
                return reject(err);
            }
        });
    }

    writeFile(dest, content) {
        return new Promise((resolve, reject) => {
            try {
                this.grunt.file.write(dest, content, {encoding: 'utf8'});
                return resolve();
            } catch (err) {
                return reject(err);
            }
        });
    }

    readFileSync(path, options) {
        try {
            return this.grunt.file.read(join(this.root, this.subpath, path), options || null);            
        } catch (err) {
            return null;
        }
    }

    readdirSync(path) {
        var fullPath = join(this.root, this.subpath, path);
        return this.grunt.file.expand(Utils.Path.join(fullPath, '*')).map(file => {
            return file.substr(fullPath.length);
        })
    }

    statSync(path) {
        const fullName = join(this.root, this.subpath, path);
        const isFile = this.grunt.file.isFile(fullName);
        const isDir = this.grunt.file.isDir(fullName);

        return {
            isDirectory: function() { return isDir; },
            isFile: function() { return isFile; }
        };
    }
}

module.exports = FsGrunt;
