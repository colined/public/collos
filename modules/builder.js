const Item = require('./item');
const Collector = require('./collector');
const Utils = require('./utils');

function renderItem(fs, out, path, item, render) {
    const outPath = Utils.Path.join(out, path);
    if (item.type === Item.Type.STATIC) {
        return fs.copyFile(outPath, item.source);
    } else if (item.type === Item.Type.TEMPLATE) {
        return render.render(item, path, null).then(content => {
            return fs.writeFile(outPath, content);
        });
    } else {
        return new Promise((resolve, reject) => { return resolve(); });
    }
}

function process(fs, log, render, config, defaults) {
    const collector = new Collector(fs, log, config.config, defaults);
    return collector.collect().then(items => {
        const promises = items.map(it => renderItem(fs, config.out, it.path, it.item, render));
        return Promise.all(promises);
    });
}

module.exports.process = process;
