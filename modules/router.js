const Config = require('./config');
const Resolver = require('./resolver');
const Utils = require('./utils');

class Router {
    constructor(fs, configFileName, defaults) {
        this.config = new Config(fs, configFileName, defaults);
        this.resolver = new Resolver(fs);
    }

    resolve(pathname) {
        const path = Utils.Path.parse(pathname);

        return this.config.resolve(path.basepath).then(config => {
            return this.resolver.resolve(path.basepath, path.path, config);
        });
    }
}

module.exports = Router;
