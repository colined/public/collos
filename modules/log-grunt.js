const colors = require('colors');

class LogGrunt {
    constructor(grunt) {
        this.grunt = grunt;
    }

    logStatic(path) {
        this.grunt.log.writeln(path + ' ' + this.grunt.log.wordlist([path], { separator: ' ', color: 'blue' }));
    }

    logTemplate(path, source, template) {
        this.grunt.log.writeln(path + ' ' + this.grunt.log.wordlist([source, template], { separator: ' ', color: 'magenta' }));
    }
}

module.exports = LogGrunt;