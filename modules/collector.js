const Item = require('./item');
const Config = require('./config');
const Resolver = require('./resolver');

const DefaultConfig = require('./default-config');
const Utils = require('./utils');

function collectDir(path, config) {
    const result = [];
    const used = {};

    config.templates.filter(template => !template.content)
        .forEach(template => {
            template.pattern.forEach(pattern => {
                const name = Utils.Path.getName(pattern);
                if (name.indexOf('?') > 0 || name.indexOf('*') >= 0)
                    return;

                if (name in used)
                    return;

                if (!Utils.Match.match(path, name, [pattern]))
                    return;

                used[name] = true;
                result.push({ path: Utils.Path.join(path, name), config: config });
            });
        });

    return result;
}

function collectFile(path, config) {
    const parsed = Utils.Path.parse(path);
    if (Utils.Match.match(parsed.basepath, parsed.path, config.static))
        return [{ path: path, config: null }];

    const result = [];
    const used = {};
    const ext = Utils.Path.getExt(path).toLowerCase();

    config.templates.filter(template => template.content === ext)
        .forEach(template => {
            template.pattern.forEach(pattern => {
                const p = Utils.Path.renameExt(path, Utils.Path.getExt(pattern));
                if (p in used)
                    return;

                used[p] = true;
                result.push({ path: p, config: config });
            });
        });

    return result;
}

function collectItems(fs, config) {
    const result = [];
    return config.resolve('/').then(cfg => {
        collectDir('/', cfg).forEach(item => result.push(item));
        const check = [{ path: '/', config: cfg }];
        return new Promise((resolve, reject) => {
            function next(index) {
                if (index === check.length) {
                    return resolve(result);
                }

                const it = check[index];
                const paths = fs.readdirSync(it.path)
                    .map(path => Utils.Path.join(it.path, path));

                const dirs = [];
                paths.forEach(p => {
                    try {
                        const stat = fs.statSync(p);
                        if (stat.isFile()) {
                            collectFile(p, it.config).forEach(item => result.push(item));
                        } else if (stat.isDirectory()) {
                            dirs.push(p)
                        }
                    } catch (err) {

                    }
                });

                const promises = dirs.map(dir => config.resolve(dir));
                return Promise.all(promises).then(configs => {
                    dirs.forEach((p, i) => {
                        const cfg = configs[i];
                        if (!Utils.Match.matchDir(p, cfg.exclude)) {
                            collectDir(Utils.Path.join(p, '/'), cfg).forEach(item => result.push(item));
                            check.push({ path: p, config: cfg });
                        }
                    });
                    next(index + 1);
                });
            }

            next(0);
        });
    });
}

class Collector {
    constructor(fs, log, configFileName, defaults) {
        this.fs = fs;
        this.log = log;
        this.config = new Config(fs, configFileName, defaults);
        this.resolver = new Resolver(fs);
    }

    collect() {
        return collectItems(this.fs, this.config).then(items => {
            const result = [];
            const promises = [];
            const paths = [];
            items.forEach(item => {
                if (item.config) {
                    const parsed = Utils.Path.parse(item.path);
                    promises.push(this.resolver.resolve(parsed.basepath, parsed.path, item.config));
                    paths.push(item.path);
                } else {
                    this.log.logStatic(item.path);
                    result.push({path: item.path, item: new Item(Item.Type.STATIC, item.path)});
                }
            });

            return Promise.all(promises).then(items => {
                return result.concat(items.map((item, index) => { 
                    if (item.type === Item.Type.TEMPLATE) {
                        this.log.logTemplate(paths[index], item.source, item.template);
                    }
                    return { path: paths[index], item }; 
                }));
            });
        });
    }
}

module.exports = Collector;