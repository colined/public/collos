const fse = require('fs-extra');
const pathResolve = require('path').resolve;
const pathParse = require('path').parse;

const Utils = require('./utils');

function join(root, subpath, path) {
    if (path && path.length > 0 && path.charAt(0) === '/') {
        return Utils.Path.join(root, path);
    }
    if (!subpath || subpath.length === 0) {
        return Utils.Path.join(root, path);
    }

    return Utils.Path.join(root, subpath, path);
}

function ensureDir(path, callback) {
    const dir = pathParse(path).dir;
    return fse.ensureDir(dir, callback);
}

class Fs {
    constructor(root, subpath) {
        this.root = pathResolve(root);
        this.subpath = subpath;
    }

    getSubpath(subpath) {
        return new Fs(this.root, subpath);
    }

    readFile(path, options) {
        return new Promise((resolve, reject) => {
            return fse.readFile(join(this.root, this.subpath, path), options || null, (err, data) => {
                if (err) {
                    return reject(err);
                } 

                return resolve(data);
            });
        });
    }

    stat(path) {
        return new Promise((resolve, reject) => {
            return fse.stat(join(this.root, this.subpath, path), (err, stat) => {
                if (err) {
                    return reject(err);
                }

                return resolve(stat);
            });
        });
    }

    copyFile(dest, path) {
        return new Promise((resolve, reject) => {
            ensureDir(dest, err => {
                if (err)
                    return reject(err);
                fse.copy(join(this.root, this.subpath, path), dest, err => {
                    if (err)
                        return reject(err);
                    return resolve();
                });
            })
        });
    }

    writeFile(dest, content) {
        return new Promise((resolve, reject) => {
            ensureDir(dest, err => {
                if (err)
                    return reject(err);
                fse.writeFile(dest, content, 'utf8', err => {
                    if (err)
                        return reject(err);
                    return resolve();
                });
            })
        });
    }

    readFileSync(path, options) {
        try {
            return fse.readFileSync(join(this.root, this.subpath, path), options);
        } catch(err) {
            return null;
        }
    }

    readdirSync(path) {
        return fse.readdirSync(join(this.root, this.subpath, path));
    }

    statSync(path) {
        return fse.statSync(join(this.root, this.subpath, path));
    }
}

module.exports = Fs;
