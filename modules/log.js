const colors = require('colors');

module.exports.logStatic = function(path) {
    console.log(path, colors.blue.bold(path));
}

module.exports.logTemplate = function(path, source, template) {
    console.log(path, colors.magenta.bold(source), colors.magenta.bold(`(template = ${template})`));
}
