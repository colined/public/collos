class DefaultConfig {
    constructor() {
        this.exclude = [
            '/**/.*/',
            '/**/.*',
            '/node_modules/',
            '/*.json'
        ];
        this.static = [
            '/**/*.png',
            '/**/*.gif',
            '/**/*.jpg',
            '/**/*.jpeg',
            '/**/*.css',
            '/favicon.ico'
        ];
        this.templates = [
            {
                pattern: ['/**/index.html'],
                template: 'index.html'
            },
            {
                pattern: ['/**/*.css'],
                content: '.scss',
                template: null
            },
            {
                pattern: ['/**/*.css'],
                content: '.sass',
                template: null
            },
            {
                pattern: ['/**/*.html'],
                content: '.md',
                template: 'template.html'
            },
            {
                pattern: ['/**/*.html'],
                content: '.md',
                template: '/template.html'
            },
            {
                pattern: ['/**/*.html'],
                content: '.md',
                template: true
            }
        ];
    }
}

module.exports = DefaultConfig;
