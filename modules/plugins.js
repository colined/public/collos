const Utils = require('./utils');

class Plugins {
    constructor() {
        this.contents = {};
        this.imports = {};
    }

    register(module) {
        if ('extension' in module) {
            this.contents[module.extension] = module;
        } else if ('namespace' in module) {
            this.imports[module.namespace] = module;
        }
    }

    parseContent(content, extension, fs, path) {
        if (extension in this.contents) {
            const parsed = Utils.Path.parse(path);
            const options = {
                fs: fs.getSubpath(parsed.basepath)
            };
            
            return this.contents[extension](content, path, options);
        } else {
            return null;
        }
    }

    getImports(fs, templatePath, path, render) {
        const result = {};
        const parsed = Utils.Path.parse(templatePath || path);
        const options = {
            parse: function(content, extension) {
                 return this.parseContent(content, extension, fs, path);
            }.bind(this),
            imports: result,
            fs: fs.getSubpath(parsed.basepath)
        };
        
        Object.keys(this.imports).forEach(key => {
            result[key] = this.imports[key](path, options);
        });
        return result;
    }
}

module.exports = Plugins;