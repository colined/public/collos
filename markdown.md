Supported Markdown
==================

Font styles
-----------
- `*italic*`, `_italic_`: *italic*
- `**bold**`, `__bold__`: **bold**
- `~~strikethrough~~`: ~~strikethrough~~
- `^sup^`: ^sup^
- `~sub~`: ~sub~
- `==mark==`: ==mark==

Lists
-----
```
1. Ordered
2. Lists
```

1. Ordered
2. Lists

```
* Unordered
* Lists
    * With
    * Sub
        * Levels
```

* Unordered
* Lists
    * With
    * Sub
        * Levels

Code
----
```
Use `back-ticks` for inlined `code`
```
Use `back-ticks` for inlined `code`

    ```
    No syntax hightlighting. Just preformated block of text
    ```

```
No syntax hightlighting. Just preformated block of text
```
  
    ```javascript
    var s = 'JavaScript syntax hightlight'
    console.log(s);
    ```

```javascript
var s = 'JavaScript syntax hightlight'
console.log(s);
```


    ```python
    def function():
        s = "Python syntax highlighting"
        print s
    ```

```python
def function():
    s = "Python syntax highlighting"
    print s
```

Tables
------
```
| Header 1 | Header 2 |
|----------|----------|
| Item 1   | Item 2   |
| *Italic* | **Bold** |
```

| Header 1 | Header 2 |
|----------|----------|
| Item 1   | Item 2   |
| *Italic* | **Bold** |

Blockquote
----------
```
> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat.
```

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat.

Notes
-----
```
::: warning
#### Warning
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::
```

::: warning
#### Warning
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::

```
::: note
#### Note
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::
```

::: note
#### Note
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::

```
::: info
#### Info
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::
```

::: info
#### Info
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::

```
::: success
#### Success
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::
```

::: success
#### Success
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
:::

Images
------
```
![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640)
```

![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640)

```
![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640 =320x)
```

![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640 =320x)

```
![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640 =160x107)
```

![](https://images.pexels.com/photos/220135/pexels-photo-220135.jpeg?w=640 =160x107)

Video
-----
```
@[youtube](UbQgXeY_zi4)
```
@[youtube](UbQgXeY_zi4)