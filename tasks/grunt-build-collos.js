'use strict'

const LogGrunt = require('../modules/log-grunt');
const FsGrunt = require('../modules/fs-grunt');
const Render = require('../modules/render');
const DefaultConfig = require('../modules/default-config');
const builder = require('../modules/builder');

module.exports = function(grunt) {
    grunt.registerMultiTask('buildcollos', 'Build collos static site.', function() {
        var options = this.options({
            path: './',
            out: './.out',
            config: 'config.json',
        });

        const defaults = new DefaultConfig();
        const fs = new FsGrunt(grunt, options.path);
        const render = new Render(fs, options);

        var done = this.async();
        builder.process(fs, new LogGrunt(grunt), render, options, defaults).then(() => done()).catch(err => {
            grunt.log.error(err.stack);
            done(err);
        });
    })
};